import 'package:dartz/dartz.dart';
import 'package:elusuario/src/features/user/data/models/request/user_not_params.dart';
import 'package:elusuario/src/features/user/domain/entities/get_users.dart';
import 'package:elusuario/src/features/user/domain/repositories/user_repository.dart';
import 'package:elusuario/src/features/user/domain/usecases/get_users_us.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'get_users_us_test.mocks.dart';

@GenerateMocks([UserRepository])
void main() {
  late GetUsersCases usecase;
  late MockUserRepository mockUserRepository;
  const userModel = GetUsers(
    email: "Sincere@april.biz",
    id: 1,
    name: "Leanne Graham",
    phone: "1-770-736-8031 x56442",
    username: "Bret",
  );
  const users = [userModel];

  setUp(() {
    mockUserRepository = MockUserRepository();
    usecase = GetUsersCases(mockUserRepository);
  });

  test(
    'Debe obtener el usuarios atraves del repositorio',
    () async {
      //arrange
      when(mockUserRepository.getUsers())
          .thenAnswer((_) async => const Right(users));
      //act
      final result = await usecase(UsersNoParams());
      //assert
      expect(result, const Right(users));
      verify(mockUserRepository.getUsers());
      verifyNoMoreInteractions(mockUserRepository);
    },
  );
}
