import 'package:dartz/dartz.dart';
import 'package:elusuario/src/features/user/data/models/local/users_local_model.dart';
import 'package:elusuario/src/features/user/data/models/request/users_local_params.dart';
import 'package:elusuario/src/features/user/domain/repositories/user_repository.dart';
import 'package:elusuario/src/features/user/domain/usecases/save_users_local_us.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'save_users_local_us_test.mocks.dart';

@GenerateMocks([UserRepository])
void main() {
  late SaveUsersLocalCases usecase;
  late MockUserRepository mockUserRepository;
  final userModel = GetUsersLocal(
    email: "Sincere@april.biz",
    id: 1,
    name: "Leanne Graham",
    phone: "1-770-736-8031 x56442",
    username: "Bret",
  );
  final users = [userModel];

  setUp(() {
    mockUserRepository = MockUserRepository();
    usecase = SaveUsersLocalCases(mockUserRepository);
  });

  test(
    'Debe guardar los usuarios en local atraves del repositorio',
    () async {
      //arrange
      when(mockUserRepository.saveUsersLocal(UsersLocalParams(users)))
          .thenAnswer((_) async => const Right(true));
      //act
      final result = await usecase(UsersLocalParams(users));
      //assert
      expect(result, const Right(true));
      verify(mockUserRepository.saveUsersLocal(UsersLocalParams(users)));
      verifyNoMoreInteractions(mockUserRepository);
    },
  );

}
