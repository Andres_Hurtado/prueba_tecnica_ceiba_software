import 'package:dartz/dartz.dart';
import 'package:elusuario/src/features/user/data/models/local/get_users_local_or_search.dart';
import 'package:elusuario/src/features/user/data/models/request/get_user_serach_params.dart';
import 'package:elusuario/src/features/user/data/models/response/get_users_model.dart';
import 'package:elusuario/src/features/user/domain/repositories/user_repository.dart';
import 'package:elusuario/src/features/user/domain/usecases/get_users_local_us.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'get_users_local_us_test.mocks.dart';

@GenerateMocks([UserRepository])
void main() {
  late GetUsersLocalCases usecase;
  late MockUserRepository mockUserRepository;
  const userModel = GetUsersModel(
    email: "Sincere@april.biz",
    id: 1,
    name: "Leanne Graham",
    phone: "1-770-736-8031 x56442",
    username: "Bret",
  );
  const users = [userModel];
  const rUsers = GetUsersLocalOrSearch(isSearch: false, users: users);

  setUp(() {
    mockUserRepository = MockUserRepository();
    usecase = GetUsersLocalCases(mockUserRepository);
  });

  test(
    'Debe obtener el usuarios atraves del repositorio data de local',
    () async {
      //arrange
      when(mockUserRepository.getUsersLocal(UsersLocalSearchParams()))
          .thenAnswer((_) async => const Right(rUsers));
      //act
      final result = await usecase(UsersLocalSearchParams());
      //assert
      expect(result, const Right(rUsers));
      verify(mockUserRepository.getUsersLocal(UsersLocalSearchParams()));
      verifyNoMoreInteractions(mockUserRepository);
    },
  );

  test(
    'Debe obtener un solo usuario al buscar atraves del repositorio data de local',
    () async {
      //arrange
      when(mockUserRepository
              .getUsersLocal(UsersLocalSearchParams(text: "Leanne Graham")))
          .thenAnswer((_) async => const Right(rUsers));
      //act
      final result =
          await usecase(UsersLocalSearchParams(text: "Leanne Graham"));
      //assert
      expect(result, const Right(rUsers));
      verify(mockUserRepository
          .getUsersLocal(UsersLocalSearchParams(text: "Leanne Graham")));
      verifyNoMoreInteractions(mockUserRepository);
    },
  );
}
