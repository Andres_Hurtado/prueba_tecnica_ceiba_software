import 'package:elusuario/src/features/post/data/models/response/get_publication_model.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../../fixtures/fixture_render.dart';

void main() {
  const publicationModel = GetPublicationIdModel(
      userId: 1,
      id: 1,
      title:
          "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
      body:
          "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto");
  const posts = [publicationModel];

  test(
    'Debe ser una subclase de la entidad GetPublicationIdModel',
    () async {
      //assert
      expect(publicationModel, isA<GetPublicationIdModel>());
    },
  );
  group("fronJson", () {
    test("Debe devolver un modelo válido cuando el ok JSON es un true",
        () async {
      //arrage
      //act
      final result = getPublicationIdFromJson(fixture("get_posts_id.json"));

      //assert
      expect(result, posts);
    });
  });
}
