import 'package:dartz/dartz.dart';
import 'package:elusuario/src/core/error/exceptions.dart';
import 'package:elusuario/src/core/error/failures.dart';
import 'package:elusuario/src/core/network/network_info.dart';
import 'package:elusuario/src/features/user/data/datasources/local/user_local_data_source.dart';
import 'package:elusuario/src/features/user/data/datasources/remote/user_remote_data_source.dart';
import 'package:elusuario/src/features/user/data/models/response/get_users_model.dart';
import 'package:elusuario/src/features/user/data/repositories/user_repository_impl.dart';
import 'package:elusuario/src/features/user/domain/entities/get_users.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../fixtures/fixture_render.dart';
import 'user_repository_impl_test.mocks.dart';

@GenerateMocks([UserRemoteDataSource, UserLocalDataSource, NetworkInfo])
void main() {
  late UserRepositoryImpl repository;
  late MockUserRemoteDataSource mockUserRemoteDataSource;
  late MockUserLocalDataSource mockUserLocalDataSource;
  late MockNetworkInfo mockNetworkInfo;

 
 final tUserModel = getUserFromJson(fixture("get_users_response.json"));
  final List<GetUsers> tUsers = tUserModel;

  setUp(() {
    mockUserRemoteDataSource = MockUserRemoteDataSource();
    mockUserLocalDataSource = MockUserLocalDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = UserRepositoryImpl(
        userRemoteDataSource: mockUserRemoteDataSource,
        userLocalDataSource: mockUserLocalDataSource,
        networkInfo: mockNetworkInfo);
  });

  group("El dispositivo está en línea", () {
    setUp(() {
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
    });

    group("El dispositivo están fuera de línea", () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
        'Debe comprobar si el dispositivo está en línea metedo getUsers',
        () async {
          //arrange
          when(mockUserRemoteDataSource.getUsers())
              .thenAnswer((_) async => tUserModel);
          //act
          final result = await repository.getUsers();
          //assert
          verify(mockUserRemoteDataSource.getUsers());
          expect(result, equals(Right(tUsers)));
        },
      );
      test(
        'Debe de devolver un mensaje de error, indicando que no hay conexion',
        () async {
          //arrange
          when(mockUserRemoteDataSource.getUsers())
              .thenThrow(ServerException(exception: CONEXION_EXCEPTION));
          //act
          final result = await repository.getUsers();
          //assert
          verify(mockUserRemoteDataSource.getUsers());
          expect(result,
              equals(Left(ServerFailure(exception: CONEXION_EXCEPTION))));
        },
      );
    });
  });
}
