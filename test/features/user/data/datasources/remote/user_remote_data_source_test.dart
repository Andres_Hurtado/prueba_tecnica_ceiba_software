
import 'package:elusuario/src/core/environment/environment.dart';
import 'package:elusuario/src/features/user/data/datasources/remote/user_remote_data_source.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../../fixtures/fixture_render.dart';
import 'user_remote_data_source_test.mocks.dart';

@GenerateMocks([http.Client])
void main() {
  late UserRemoteDataSourceImpl dataSource;
  late MockClient mockHttpClient;

  void setUpMockHttpClientSuccess200() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
      (_) async => http.Response(fixture('get_users.json'), 200),
    );
  }

  setUp(() {
    mockHttpClient = MockClient();
    dataSource = UserRemoteDataSourceImpl(client: mockHttpClient);
  });

  group("Users", () {
    // final tGetUser = getUserFromJson(json.decode(fixture("get_users.json")));
    test('''Debe realizar una solicitud GET en una URL
        como punto final y con el encabezado application/json''', () async {
      //arrange
      setUpMockHttpClientSuccess200();

      //act
      dataSource.getUsers();

      //assert
      verifyNever(mockHttpClient.get(Uri(query: "$environment_url_api/users"),
          headers: {"Content-Type": "application/json"}));
    });
  });
}
