// Mocks generated by Mockito 5.3.2 from annotations
// in elusuario/test/features/user/presentation/bloc/user/user_bloc_test.dart.
// Do not manually edit this file.

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'dart:async' as _i4;

import 'package:dartz/dartz.dart' as _i2;
import 'package:elusuario/src/core/error/failures.dart' as _i5;
import 'package:elusuario/src/features/user/data/models/local/get_users_local_or_search.dart'
    as _i11;
import 'package:elusuario/src/features/user/data/models/request/get_user_serach_params.dart'
    as _i12;
import 'package:elusuario/src/features/user/data/models/request/user_not_params.dart'
    as _i7;
import 'package:elusuario/src/features/user/data/models/request/users_local_params.dart'
    as _i9;
import 'package:elusuario/src/features/user/domain/entities/get_users.dart'
    as _i6;
import 'package:elusuario/src/features/user/domain/usecases/get_users_local_us.dart'
    as _i10;
import 'package:elusuario/src/features/user/domain/usecases/get_users_us.dart'
    as _i3;
import 'package:elusuario/src/features/user/domain/usecases/save_users_local_us.dart'
    as _i8;
import 'package:mockito/mockito.dart' as _i1;

// ignore_for_file: type=lint
// ignore_for_file: avoid_redundant_argument_values
// ignore_for_file: avoid_setters_without_getters
// ignore_for_file: comment_references
// ignore_for_file: implementation_imports
// ignore_for_file: invalid_use_of_visible_for_testing_member
// ignore_for_file: prefer_const_constructors
// ignore_for_file: unnecessary_parenthesis
// ignore_for_file: camel_case_types
// ignore_for_file: subtype_of_sealed_class

class _FakeEither_0<L, R> extends _i1.SmartFake implements _i2.Either<L, R> {
  _FakeEither_0(
    Object parent,
    Invocation parentInvocation,
  ) : super(
          parent,
          parentInvocation,
        );
}

/// A class which mocks [GetUsersCases].
///
/// See the documentation for Mockito's code generation for more information.
class MockGetUsersCases extends _i1.Mock implements _i3.GetUsersCases {
  MockGetUsersCases() {
    _i1.throwOnMissingStub(this);
  }

  @override
  _i4.Future<_i2.Either<_i5.Failure, List<_i6.GetUsers>>> call(
          _i7.UsersNoParams? param) =>
      (super.noSuchMethod(
        Invocation.method(
          #call,
          [param],
        ),
        returnValue:
            _i4.Future<_i2.Either<_i5.Failure, List<_i6.GetUsers>>>.value(
                _FakeEither_0<_i5.Failure, List<_i6.GetUsers>>(
          this,
          Invocation.method(
            #call,
            [param],
          ),
        )),
      ) as _i4.Future<_i2.Either<_i5.Failure, List<_i6.GetUsers>>>);
}

/// A class which mocks [SaveUsersLocalCases].
///
/// See the documentation for Mockito's code generation for more information.
class MockSaveUsersLocalCases extends _i1.Mock
    implements _i8.SaveUsersLocalCases {
  MockSaveUsersLocalCases() {
    _i1.throwOnMissingStub(this);
  }

  @override
  _i4.Future<_i2.Either<_i5.Failure, bool>> call(_i9.UsersLocalParams? param) =>
      (super.noSuchMethod(
        Invocation.method(
          #call,
          [param],
        ),
        returnValue: _i4.Future<_i2.Either<_i5.Failure, bool>>.value(
            _FakeEither_0<_i5.Failure, bool>(
          this,
          Invocation.method(
            #call,
            [param],
          ),
        )),
      ) as _i4.Future<_i2.Either<_i5.Failure, bool>>);
}

/// A class which mocks [GetUsersLocalCases].
///
/// See the documentation for Mockito's code generation for more information.
class MockGetUsersLocalCases extends _i1.Mock
    implements _i10.GetUsersLocalCases {
  MockGetUsersLocalCases() {
    _i1.throwOnMissingStub(this);
  }

  @override
  _i4.Future<_i2.Either<_i5.Failure, _i11.GetUsersLocalOrSearch>> call(
          _i12.UsersLocalSearchParams? param) =>
      (super.noSuchMethod(
        Invocation.method(
          #call,
          [param],
        ),
        returnValue: _i4.Future<
                _i2.Either<_i5.Failure, _i11.GetUsersLocalOrSearch>>.value(
            _FakeEither_0<_i5.Failure, _i11.GetUsersLocalOrSearch>(
          this,
          Invocation.method(
            #call,
            [param],
          ),
        )),
      ) as _i4.Future<_i2.Either<_i5.Failure, _i11.GetUsersLocalOrSearch>>);
}
