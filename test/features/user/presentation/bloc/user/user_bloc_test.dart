import 'package:dartz/dartz.dart';
import 'package:elusuario/src/core/error/failures.dart';
import 'package:elusuario/src/features/user/data/models/local/get_users_local_or_search.dart';
import 'package:elusuario/src/features/user/data/models/request/get_user_serach_params.dart';
import 'package:elusuario/src/features/user/data/models/request/user_not_params.dart';
import 'package:elusuario/src/features/user/data/models/response/get_users_model.dart';
import 'package:elusuario/src/features/user/domain/usecases/get_users_local_us.dart';
import 'package:elusuario/src/features/user/domain/usecases/get_users_us.dart';
import 'package:elusuario/src/features/user/domain/usecases/save_users_local_us.dart';
import 'package:elusuario/src/features/user/presentation/bloc/user/user_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'user_bloc_test.mocks.dart';

@GenerateMocks([
  GetUsersCases,
  SaveUsersLocalCases,
  GetUsersLocalCases,
])
void main() {
  late UserBloc bloc;
  late MockGetUsersCases mockGetUsersCases;
  late MockSaveUsersLocalCases mockSaveUsersLocalCases;
  late MockGetUsersLocalCases mockGetUsersLocalCases;

  const getUsers = GetUsersModel(
    email: "Sincere@april.biz",
    id: 1,
    name: "Leanne Graham",
    phone: "1-770-736-8031 x56442",
    username: "Bret",
  );
  const users = [getUsers];
  final rUsers = GetUsersLocalOrSearch(users: users, isSearch: false);

  setUp(() {
    mockGetUsersCases = MockGetUsersCases();
    mockSaveUsersLocalCases = MockSaveUsersLocalCases();
    mockGetUsersLocalCases = MockGetUsersLocalCases();

    bloc = UserBloc(
      getUsersCases: mockGetUsersCases,
      getUsersLocalCases: mockGetUsersLocalCases,
      saveUsersLocalCases: mockSaveUsersLocalCases,
    );
  });

  test('Bloc debe estar en su estado inicial UserInitial', () {
    expect(bloc.state, isA<UserInitial>());
  });

  group("Bloc user data", () {
    test("Debe obtener datos del caso de uso getUsersLocalCases", () async* {
      // arrange
      when(mockGetUsersLocalCases(any)).thenAnswer((_) async => Right(rUsers));
      //act
      bloc.add(GetUserLocalEevent(null));
      await untilCalled(mockGetUsersLocalCases(any));

      // //assert
      verify(mockGetUsersLocalCases(UsersLocalSearchParams()));
    });

    test("Debe obtener datos del caso de uso getUsersCases", () async* {
      // arrange
      when(mockGetUsersCases(any)).thenAnswer((_) async => Right(users));
      //act
      bloc.add(GetUserLocalEevent(null));
      await untilCalled(mockGetUsersCases(any));

      // //assert
      verify(mockGetUsersCases(UsersNoParams()));
    });

    test(
      'Debe emitir los Estados [Inicial, Cargando, Cargado] cuando los datos se obtienen con éxito',
      () async* {
        // arrange
        when(mockGetUsersCases(any)).thenAnswer((_) async => Right(users));

        // assert later
        final expected = [
          UserInitial(),
          LoadingGetUser(),
          LoadedGetUser(users: users),
        ];
        expectLater(bloc, emitsInOrder(expected));
        // act
        bloc.add(GetUserEevent());
      },
    );

    test(
      'Debe emitir [Cargando, error] cuando los datos se obtienen con error',
      () async* {
        // arrange
        when(mockGetUsersCases(any))
            .thenAnswer((_) async => Left(ServerFailure(exception: "Error")));

        // assert later
        final expected = [
          UserInitial(),
          LoadingGetUser(),
          ExceptionGetUser(message: "Error")
        ];
        expectLater(bloc, emitsInOrder(expected));
        // act
        bloc.add(GetUserEevent());
      },
    );
  });
}
