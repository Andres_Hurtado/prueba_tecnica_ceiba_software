import 'package:dartz/dartz.dart';
import 'package:elusuario/src/core/error/failures.dart';
import 'package:elusuario/src/features/post/data/models/request/get_publication_request.dart';
import 'package:elusuario/src/features/post/domain/entities/get_publication_uid.dart';
import 'package:elusuario/src/features/post/domain/usecases/get_publication_uid_us.dart';
import 'package:elusuario/src/features/post/presentation/bloc/publication/publication_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'publication_bloc_test.mocks.dart';

@GenerateMocks([
  GetPublicationUidCases,
])
void main() {
  late PublicationBloc bloc;
  late MockGetPublicationUidCases mockGetPublicationUidCases;

  const getPosts = GetPublicationId(
    id: 1,
    userId: 1,
    title:
        "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    body:
        "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto",
  );
  const posts = [getPosts];
  const userId = 1;

  setUp(() {
    mockGetPublicationUidCases = MockGetPublicationUidCases();

    bloc = PublicationBloc(
      getPublicationUidCases: mockGetPublicationUidCases,
    );
  });

  test('Bloc debe estar en su estado inicial PublicationInitial', () {
    expect(bloc.state, isA<PublicationInitial>());
  });

  group("Bloc publication data", () {
    test("Debe obtener datos del caso de uso getPublicationUidCases",
        () async* {
      // arrange
      when(mockGetPublicationUidCases(any))
          .thenAnswer((_) async => Right(posts));
      //act
      bloc.add(GetPublicationUidEvent(userId));
      await untilCalled(mockGetPublicationUidCases(any));

      // //assert
      verify(mockGetPublicationUidCases(GetPublicationParams(userId: userId)));
    });

    test(
      'Debe emitir los Estados [Inicial, Cargando, Cargado] cuando los datos se obtienen con éxito',
      () async* {
        // arrange
        when(mockGetPublicationUidCases(any))
            .thenAnswer((_) async => Right(posts));

        // assert later
        final expected = [
          PublicationInitial(),
          LoadingGetPublicationUid(),
          LoadedGetPublicationUid(posts: posts),
        ];
        expectLater(bloc, emitsInOrder(expected));
        // act
        bloc.add(GetPublicationUidEvent(userId));
      },
    );

    test(
      'Debe emitir [Cargando, error] cuando los datos se obtienen con error',
      () async* {
        // arrange
        when(mockGetPublicationUidCases(any))
            .thenAnswer((_) async => Left(ServerFailure(exception: "")));

        // assert later
        final expected = [
          PublicationInitial(),
          LoadingGetPublicationUid(),
          ExceptionGetPublicationUid(message: "")
        ];
        expectLater(bloc, emitsInOrder(expected));
        // act
        bloc.add(GetPublicationUidEvent(userId));
      },
    );
  });
}
