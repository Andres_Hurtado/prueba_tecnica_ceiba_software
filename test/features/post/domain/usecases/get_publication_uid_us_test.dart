import 'package:dartz/dartz.dart';
import 'package:elusuario/src/features/post/data/models/request/get_publication_request.dart';
import 'package:elusuario/src/features/post/data/models/response/get_publication_model.dart';
import 'package:elusuario/src/features/post/domain/entities/get_publication_uid.dart';
import 'package:elusuario/src/features/post/domain/repositories/publication_repository.dart';
import 'package:elusuario/src/features/post/domain/usecases/get_publication_uid_us.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../fixtures/fixture_render.dart';
import 'get_publication_uid_us_test.mocks.dart';

@GenerateMocks([PublicationRepository])
void main() {
  late GetPublicationUidCases usecase;
  late MockPublicationRepository mockPublicationRepository;

  var posts = getPublicationIdFromJson(fixture("get_posts_id.json")).toList();
  List<GetPublicationId> rPosts = posts;

  setUp(() {
    mockPublicationRepository = MockPublicationRepository();
    usecase = GetPublicationUidCases(mockPublicationRepository);
  });
  const userId = 1;
  test(
    'Debe obtener las publicaciones por id atraves del repositorio',
    () async {
      //arrange
      when(mockPublicationRepository
              .getPPublications(GetPublicationParams(userId: userId)))
          .thenAnswer((_) async => Right(rPosts));
      //act
      final result = await usecase(GetPublicationParams(userId: userId));
      //assert
      expect(result, Right(rPosts));
      verify(mockPublicationRepository
          .getPPublications(GetPublicationParams(userId: userId)));
      verifyNoMoreInteractions(mockPublicationRepository);
    },
  );
}
