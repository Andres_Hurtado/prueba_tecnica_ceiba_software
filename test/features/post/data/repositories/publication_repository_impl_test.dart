import 'package:dartz/dartz.dart';
import 'package:elusuario/src/core/error/exceptions.dart';
import 'package:elusuario/src/core/error/failures.dart';
import 'package:elusuario/src/core/network/network_info.dart';
import 'package:elusuario/src/features/post/data/datasources/remote/publication_remote_data_sources.dart';
import 'package:elusuario/src/features/post/data/models/request/get_publication_request.dart';
import 'package:elusuario/src/features/post/data/models/response/get_publication_model.dart';
import 'package:elusuario/src/features/post/data/repositories/publication_repository_impl.dart';
import 'package:elusuario/src/features/post/domain/entities/get_publication_uid.dart';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../fixtures/fixture_render.dart';
import 'publication_repository_impl_test.mocks.dart';

@GenerateMocks([PublicationRemoteDataSource, NetworkInfo])
void main() {
  late PublicationRepositoryImpl repository;
  late MockPublicationRemoteDataSource mockPublicationRemoteDataSource;
  late MockNetworkInfo mockNetworkInfo;

  final tPublicationId = getPublicationIdFromJson(fixture("get_posts_id_response.json"));
  final List<GetPublicationId> tPublications = tPublicationId;

  const userId = 1;

  setUp(() {
    mockPublicationRemoteDataSource = MockPublicationRemoteDataSource();
    mockNetworkInfo = MockNetworkInfo();
    repository = PublicationRepositoryImpl(
        publicationRemoteDataSource: mockPublicationRemoteDataSource,
        networkInfo: mockNetworkInfo);
  });

  group("El dispositivo está en línea publicacion", () {
    setUp(() {
      when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
    });

    group("El dispositivo están fuera de línea publicacion", () {
      setUp(() {
        when(mockNetworkInfo.isConnected).thenAnswer((_) async => true);
      });

      test(
        'Debe comprobar si el dispositivo está en línea metedo getPublications',
        () async {
          //arrange
          when(mockPublicationRemoteDataSource
                  .getPublications(GetPublicationParams(userId: userId)))
              .thenAnswer((_) async => tPublicationId);
          //act
          final result = await repository
              .getPPublications(GetPublicationParams(userId: userId));
          //assert
          verify(mockPublicationRemoteDataSource
              .getPublications(GetPublicationParams(userId: userId)));
          expect(result, equals(Right(tPublications)));
        },
      );
      test(
        'Debe de devolver un mensaje de error, indicando que no hay conexion',
        () async {
          //arrange
          when(mockPublicationRemoteDataSource
                  .getPublications(GetPublicationParams(userId: userId)))
              .thenThrow(ServerException(exception: CONEXION_EXCEPTION));
          //act
          final result = await repository
              .getPPublications(GetPublicationParams(userId: userId));
          //assert
          verify(mockPublicationRemoteDataSource
              .getPublications(GetPublicationParams(userId: userId)));
          expect(result,
              equals(Left(ServerFailure(exception: CONEXION_EXCEPTION))));
        },
      );
    });
  });
}
