
import 'package:elusuario/src/core/environment/environment.dart';
import 'package:elusuario/src/features/post/data/datasources/remote/publication_remote_data_sources.dart';
import 'package:elusuario/src/features/post/data/models/request/get_publication_request.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart' as http;
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../../../../fixtures/fixture_render.dart';
import 'publication_remote_data_sources_test.mocks.dart';

@GenerateMocks([http.Client])
void main() {
  late PublicationRemoteDataSourceImpl dataSource;
  late MockClient mockHttpClient;

  void setUpMockHttpClientSuccess200() {
    when(mockHttpClient.get(any, headers: anyNamed('headers'))).thenAnswer(
      (_) async => http.Response(fixture('get_posts_id.json'), 200),
    );
  }
  
  const userId = 1;

  setUp(() {
    mockHttpClient = MockClient();
    dataSource = PublicationRemoteDataSourceImpl(client: mockHttpClient);
  });

  group("Posts", () {
    test('''Debe realizar una solicitud GET en una URL
        como punto final y con el encabezado application/json''', () async {
      //arrange
      setUpMockHttpClientSuccess200();

      //act
      dataSource.getPublications(GetPublicationParams(userId: userId));

      //assert
      verifyNever(mockHttpClient.get(Uri(query: "$environment_url_api/posts?userId=$userId"),
          headers: {"Content-Type": "application/json"}));
    });
  });
}
