import 'package:elusuario/src/features/user/data/models/response/get_users_model.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../../../../fixtures/fixture_render.dart';

void main() {
  const userModel = GetUsersModel(
    email: "Sincere@april.biz",
    id: 1,
    name: "Leanne Graham",
    phone: "1-770-736-8031 x56442",
    username: "Bret",
  );
  const users = [userModel];

  test(
    'Debe ser una subclase de la entidad GetUserModel',
    () async {
      //assert
      expect(userModel, isA<GetUsersModel>());
    },
  );
  group("fronJson", () {
    test("Debe devolver un modelo válido cuando el ok JSON es un true",
        () async {
      //arrage
      //act
      final result = getUserFromJson(fixture("get_users.json"));

      //assert
      expect(result, users);
    });

  });
}
