import 'package:data_connection_checker_nulls/data_connection_checker_nulls.dart';
import 'package:elusuario/src/core/network/network_info.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'network_info_test.mocks.dart';

@GenerateMocks([DataConnectionChecker])
void main() {
  late NetworkInfoImpl networkInfo;
  late MockDataConnectionChecker mockUDataConnectionChecker;

  setUp(() {
    mockUDataConnectionChecker = MockDataConnectionChecker();
    networkInfo = NetworkInfoImpl(mockUDataConnectionChecker);
  });

  group("Esta conectado", () {
    test(
      'Debe reenviar la llamada a DataConnectionChecker.hasConnection',
      () async {
        final tHasConnectionFuture = Future.value(true);
        //arrange
        when(mockUDataConnectionChecker.hasConnection)
            .thenAnswer((_) => tHasConnectionFuture);
        //act
        final result = networkInfo.isConnected;
        //assert
        verify(mockUDataConnectionChecker.hasConnection);
        expect(result, tHasConnectionFuture);
      },
    );
  });
}
