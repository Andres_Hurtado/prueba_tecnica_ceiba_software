import 'package:flutter/material.dart';

import 'src/core/framework/colors.dart';

mixin AppTheme {
  static ThemeData buildTheme() => ThemeData(
        primaryColor: PRIMARY_COLOR,
        colorScheme: const ColorScheme(
            brightness: Brightness.light,
            primary: PRIMARY_COLOR,
            onPrimary: PRIMARY_COLOR,
            secondary: SECONDARY_COLOR,
            onSecondary: SECONDARY_COLOR,
            error: ERROR_COLOR,
            onError: BACKGROUND_COLOR,
            background: BACKGROUND_COLOR,
            onBackground: BACKGROUND_COLOR,
            surface: SURFACE_COLOR,
            onSurface: SURFACE_COLOR),
        brightness: Brightness.light,
        backgroundColor: BACKGROUND_COLOR,
        //! ===================================================
        //! Tema Fuente principal
        //! ===================================================
        fontFamily: "RubikStaticMedium",
        //! ===================================================
        //! Tema de texto
        //! ===================================================
        textTheme: const TextTheme(
          titleLarge: TextStyle(
              color: SECONDARY_TEXT_COLOR,
              fontSize: 30,
              fontFamily: 'RubikStaticMedium'),
          titleMedium: TextStyle(
              color: PRIMARY_TEXT_COLOR,
              fontSize: 20,
              fontFamily: 'RubikStaticMedium'),
          titleSmall: TextStyle(
              color: SECONDARY_TEXT_LIGH_COLOR,
              fontSize: 14,
              fontFamily: 'RubikStaticMedium'),
          labelLarge: TextStyle(
              color: SECONDARY_TEXT_LIGH_COLOR,
              fontSize: 17,
              fontFamily: 'RubikStaticLight'),
          labelMedium: TextStyle(
              color: SECONDARY_TEXT_LIGH_COLOR,
              fontSize: 14,
              fontFamily: 'RubikStaticLight'),
          labelSmall: TextStyle(
              color: SECONDARY_TEXT_DART_COLOR,
              fontSize: 14,
              fontFamily: 'RubikStaticRegular'),
          bodyLarge: TextStyle(
              color: BLACK_COLOR, fontSize: 20, fontFamily: 'RubikStaticLight'),
          bodyMedium: TextStyle(
              color: BLACK_COLOR, fontSize: 14, fontFamily: 'RubikStaticLight'),
          bodySmall: TextStyle(
              color: BLACK_COLOR, fontSize: 12, fontFamily: 'RubikStaticLight'),
        ),
        //! ===================================================
        //! Tema de botones
        //! ===================================================
        buttonTheme: const ButtonThemeData(
            buttonColor: SURFACE_COLOR, textTheme: ButtonTextTheme.primary),
        textButtonTheme: TextButtonThemeData(
            style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all(SURFACE_COLOR),
          textStyle: MaterialStateProperty.all(const TextStyle(
              color: WHITE_COLOR,
              fontSize: 14,
              // fontWeight: FontWeight.bold,
              fontFamily: "RubikStaticMedium")),
          shape: MaterialStateProperty.all(
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          ),
          minimumSize: MaterialStateProperty.all(const Size(357, 55)),
          foregroundColor: MaterialStateProperty.all(WHITE_COLOR),
        )),
        iconTheme: const IconThemeData(color: SECONDARY_COLOR),
      );
}
