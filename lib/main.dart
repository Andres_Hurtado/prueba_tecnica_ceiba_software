import 'package:flutter/material.dart';
import 'package:hive/hive.dart';

import 'injection_container.dart' as di;
import 'src/app.dart';
import 'src/features/user/data/models/local/users_local_model.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //! Inicializamos el apdater, el cual va permitir
  //! Manejar esos objectos en Hive, de acuerdo a la
  //! Estructura de la clase GetUsersLocal
  Hive.registerAdapter(GetUsersLocalAdapter());
  //! Inicializamos nuestras dependencias
  await di.init();
  runApp(const MyInitializer());
}
