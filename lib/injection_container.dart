import 'package:data_connection_checker_nulls/data_connection_checker_nulls.dart';
import 'package:get_it/get_it.dart';
import 'package:http/http.dart' as http;

import 'src/core/inilicilize_hive_box/inilicialize_hive_box.dart';
import 'src/core/network/network_info.dart';
import 'src/features/post/data/datasources/remote/publication_remote_data_sources.dart';
import 'src/features/post/data/repositories/publication_repository_impl.dart';
import 'src/features/post/domain/repositories/publication_repository.dart';
import 'src/features/post/domain/usecases/get_publication_uid_us.dart';
import 'src/features/post/presentation/bloc/publication/publication_bloc.dart';
import 'src/features/user/data/datasources/local/user_local_data_source.dart';
import 'src/features/user/data/datasources/remote/user_remote_data_source.dart';
import 'src/features/user/data/repositories/user_repository_impl.dart';
import 'src/features/user/domain/repositories/user_repository.dart';
import 'src/features/user/domain/usecases/get_users_local_us.dart';
import 'src/features/user/domain/usecases/get_users_us.dart';
import 'src/features/user/domain/usecases/save_users_local_us.dart';
import 'src/features/user/presentation/bloc/user/user_bloc.dart';

final sl = GetIt.instance;
Future<void> init() async {
  //! Features
  initFeatureUser();
  initFeaturePublicationPosts();
  //! Core
  initCore();
  //! External
  initExternal();
}

void initFeatureUser() {
  // Bloc
  sl.registerFactory(
    () => UserBloc(
      getUsersCases: sl(),
      getUsersLocalCases: sl(),
      saveUsersLocalCases: sl(),
    ),
  );

  // Use cases
  sl.registerLazySingleton(() => GetUsersCases(sl()));
  sl.registerLazySingleton(() => GetUsersLocalCases(sl()));
  sl.registerLazySingleton(() => SaveUsersLocalCases(sl()));

  // Repository
  sl.registerLazySingleton<UserRepository>(() => UserRepositoryImpl(
        userRemoteDataSource: sl(),
        userLocalDataSource: sl(),
        networkInfo: sl(),
      ));
  //data sources
  sl.registerLazySingleton<UserRemoteDataSource>(
    () => UserRemoteDataSourceImpl(
      client: sl(),
    ),
  );

  sl.registerLazySingleton<UserLocalDataSource>(
    () => UserLocalDataSourceeImpl(initializeBox: sl()),
  );
}

void initFeaturePublicationPosts() {
  // Bloc
  sl.registerFactory(
    () => PublicationBloc(
      getPublicationUidCases: sl(),
    ),
  );

  // Use cases
  sl.registerLazySingleton(() => GetPublicationUidCases(sl()));

  // Repository
  sl.registerLazySingleton<PublicationRepository>(
      () => PublicationRepositoryImpl(
            publicationRemoteDataSource: sl(),
            networkInfo: sl(),
          ));
  //data sources
  sl.registerLazySingleton<PublicationRemoteDataSource>(
    () => PublicationRemoteDataSourceImpl(
      client: sl(),
    ),
  );
}

void initCore() {
  sl.registerLazySingleton<NetworkInfo>(() => NetworkInfoImpl(sl()));
  sl.registerLazySingleton<InitializeBox>(InitializeBoxImpl.new);
}

void initExternal() async {
  sl.registerLazySingleton(http.Client.new);
  sl.registerLazySingleton(DataConnectionChecker.new);
}
