import 'package:flutter/material.dart';

class CustomUserDetailItem extends StatelessWidget {
  const CustomUserDetailItem({
    Key? key,
    required this.icon,
    required this.data,
  }) : super(key: key);

  final String data;
  final IconData icon;

  @override
  Widget build(BuildContext context) {
    final themedata = Theme.of(context);
    return Row(
      children: [
        Icon(
          icon,
          color: themedata.primaryColor,
        ),
        SizedBox(width: 10),
        Text(data),
      ],
    );
  }
}
