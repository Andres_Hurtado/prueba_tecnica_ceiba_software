import 'package:flutter/material.dart';

const PRIMARY_COLOR = Color(0xFF2e7d32);
const SECONDARY_COLOR = Color(0xFFf1f8e9);

const PRIMARY_TEXT_COLOR = Color(0xFFffffff);
const SECONDARY_TEXT_COLOR = Color(0xFF000000);
const SECONDARY_TEXT_LIGH_COLOR = Color(0xFFffffff);
const SECONDARY_TEXT_DART_COLOR = Color(0xFFbec5b7);

const BACKGROUND_COLOR = Color.fromARGB(255, 237, 237, 237);

const SURFACE_COLOR = Color(0xFF13497B);

const WHITE_COLOR = Color(0xFFFFFFFF);
const BLACK_COLOR = Color(0xFF020202);

const ERROR_COLOR = Color(0xFFE83F67);

const PRIMARY_ICON_COLOR = Color(0xFF000000);

const SHADOW_COLOR = Color(0XFF262B52);
const BORDER_COLOR = Color(0xFFB3B3B3);
const CONTENT_BODY_BACKGROUND_COLOR = Color(0xFFB3B3B3);

const LOADING_BOX_COLOR = Color.fromARGB(89, 211, 219, 226);
