import 'package:flutter/material.dart';

import 'colors.dart';

// ignore: non_constant_identifier_names
final BOX_SHADOW = BoxShadow(
  color: SHADOW_COLOR.withOpacity(0.05),
  spreadRadius: 5,
  blurRadius: 7,
  offset: Offset(0, 3), // changes position of shadow
);
