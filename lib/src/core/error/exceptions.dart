
class ServerException implements Exception {
  const ServerException({required this.exception});
  final String exception;
}

class LocalException implements Exception {
  const LocalException({required this.exception});
  final String exception;
}
