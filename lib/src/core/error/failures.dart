import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  const Failure({
    required this.exception,
  });
  final String exception;
  @override
  List<Object> get props => [exception];
}

/// General failures
class ServerFailure extends Failure {
  const ServerFailure({required String exception})
      : super(exception: exception);
}

class LocalFailure extends Failure {
  const LocalFailure({required String exception})
      : super(exception: exception);
}
