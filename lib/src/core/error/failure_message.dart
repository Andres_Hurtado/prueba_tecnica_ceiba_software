import 'failures.dart';

String failureToMessage(Failure failure) {
  switch (failure.runtimeType) {
    case ServerFailure:
      return failure.exception;
    case LocalFailure:
      return failure.exception;
    default:
      return 'Error inesperado';
  }
}

