import 'package:data_connection_checker_nulls/data_connection_checker_nulls.dart';


const CONEXION_EXCEPTION = "No se ha podido establecer conexión con Internet";

abstract class NetworkInfo {
  Future<bool> get isConnected;
}

class NetworkInfoImpl extends NetworkInfo {
  final DataConnectionChecker _dataConnectionChecker;

  NetworkInfoImpl(this._dataConnectionChecker);

  @override
  Future<bool> get isConnected => _dataConnectionChecker.hasConnection;
}
