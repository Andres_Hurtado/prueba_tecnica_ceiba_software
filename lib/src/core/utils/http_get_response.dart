import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:http/http.dart' as http;

import '../environment/environment.dart';
import '../error/exceptions.dart';
import '../error/failures.dart';
import '../network/network_info.dart';

mixin Utils {
  static Future<T> responseDataSourceTempl<T>(
    http.Response response, {
    required T data,
  }) async {
    if (response.statusCode == 200) {
      return data;
    } else {
      throw ServerException(exception: json.decode(response.body));
    }
  }

  static Future<Either<Failure, T>> responseRepositoryTempl<T>({
    required Function method,
    dynamic param,
    NetworkInfo? networkInfo,
  }) async {
    try {
      if (networkInfo != null && await networkInfo.isConnected) {
        return Right(param == null ? await method() : await method(param));
      } else {
        return Left(
          ServerFailure(
            exception: CONEXION_EXCEPTION,
          ),
        );
      }
    } on ServerException catch (e) {
      return Left(ServerFailure(exception: e.exception));
    }
  }

  static Future<Either<Failure, T>> responseRepositoryLocalTempl<T>({
    required Function method,
    dynamic param,
  }) async {
    try {
      return Right(param == null ? await method() : await method(param));
    } on ServerException catch (e) {
      return Left(ServerFailure(exception: e.exception));
    }
  }

  static Future<http.Response> callGetClient(
    http.Client client, {
    required String endpoint,
  }) async {
    try {
      final uri = Uri.parse("$environment_url_api/$endpoint");
      return await client.get(
        uri,
        headers: {
          "Content-Type": "application/json",
        },
      );
    } on http.ClientException catch (_) {
      throw ServerException(exception: CONEXION_EXCEPTION);
    }
  }
}
