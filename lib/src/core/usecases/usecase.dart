import 'package:dartz/dartz.dart';

import '../error/failures.dart';

abstract class UseCase<type, params> {
  Future<Either<Failure, type>> call(params params) ;
}
