import 'dart:io';

import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';

import '../error/failures.dart';

abstract class InitializeBox {
  Future<Box<dynamic>> get initializeBox;
}

class InitializeBoxImpl extends InitializeBox {
  Future<Box<dynamic>> initializeBoxx() async {
    try {
      var appDocDir = await getApplicationSupportDirectory();
      Hive.init(appDocDir.path);
      return await Hive.openBox("usersBox");
    } on FileSystemException catch (_) {
      throw LocalFailure(exception: "Errror al inicializar la BD local");
    }
  }

  @override
  Future<Box> get initializeBox async => await initializeBoxx();
}
