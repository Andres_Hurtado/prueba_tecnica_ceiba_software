import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../injection_container.dart';
import '../theme.dart';
import 'features/post/presentation/bloc/publication/publication_bloc.dart';
import 'features/user/presentation/bloc/user/user_bloc.dart';
import 'routes/app_route_screens.dart';
import 'routes/app_routes.dart';

class MyInitializer extends StatelessWidget {
  const MyInitializer({super.key});
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<UserBloc>(
          create: (_) => sl<UserBloc>(),
        ),
        BlocProvider<PublicationBloc>(
          create: (_) => sl<PublicationBloc>(),
        ),
      ],
      child: MyApp(),
    );
  }
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.transparent,
      statusBarIconBrightness: Brightness.dark,
    ));
    return MaterialApp(
      title: 'El_Usuario',
      debugShowCheckedModeBanner: false,
      theme: AppTheme.buildTheme(),
      initialRoute: AppRoutes.splash,
      routes: appRoutes,
    );
  }
}
