import 'package:flutter/material.dart';

import '../features/post/presentation/routes/user_route_screens.dart';
import '../features/splash/presentation/splash/splash_screen.dart';
import '../features/user/presentation/routes/user_route_screens.dart';
import 'app_routes.dart';

//! Ruta global
//! NOTA: podria tranquilamente crear una sola ruta global,
//! y hay gestionar todas mis rutas, pero siento que como lo estoy
//! haciendo, es mucho mas organizado ya que esta todo por Features
final appRoutes = <String, WidgetBuilder>{
  AppRoutes.splash: (_) => SplashScreen(),
  ...userRoutes,
  ...publicationsRoutes,
};
