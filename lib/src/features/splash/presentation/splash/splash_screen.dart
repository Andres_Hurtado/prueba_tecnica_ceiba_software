import 'package:flutter/material.dart';

import '../../../user/presentation/routes/user_routes.dart';
import 'components/custom_plash_logo.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      body: FutureBuilder<Object?>(
          future: splashState(context),
          builder: (context, snapshot) {
            return CustomLogoSplash(
              size: size,
            );
          }),
    );
  }

  Future<void> splashState(BuildContext context) async {
    await Future.delayed(Duration(milliseconds: 3500));
    Navigator.pushNamedAndRemoveUntil(
      context,
      Routes.home,
      (route) => false,
    );
  }
}
