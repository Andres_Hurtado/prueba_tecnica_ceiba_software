
import 'package:animator/animator.dart';
import 'package:flutter/material.dart';

class CustomLogoSplash extends StatelessWidget {
  const CustomLogoSplash({
    Key? key,
    required this.size,
  }) : super(key: key);

  final Size size;

  @override
  Widget build(BuildContext context) {
    return Animator<double>(
      tween: Tween<double>(begin: 0, end: 300),
      curve: Curves.fastLinearToSlowEaseIn,
      duration: const Duration(milliseconds: 700),
      cycles: 0,
      builder: (context, animatorState, child) => Center(
        child: SizedBox(
          width: animatorState.value,
          height: animatorState.value,
          child: Image.asset(
            "assets/logo.png",
            width: 200,
          ),
        ),
      ),
    );
  }
}
