import 'package:flutter/material.dart';

import '../screens/publications/publications_screen.dart';
import 'user_routes.dart';

final publicationsRoutes = <String, WidgetBuilder>{
  PublicationsRoutes.publications: (_) => PublicationScreen(),
};
