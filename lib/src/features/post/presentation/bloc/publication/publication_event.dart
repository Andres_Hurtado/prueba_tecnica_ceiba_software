part of 'publication_bloc.dart';

abstract class PublicationEvent extends Equatable {
  const PublicationEvent();

  @override
  List<Object> get props => [];
}

class GetPublicationUidEvent extends PublicationEvent {
  GetPublicationUidEvent(this.userId);
  final int userId;
  @override
  List<Object> get props => [userId];
}
