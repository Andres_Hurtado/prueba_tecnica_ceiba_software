part of 'publication_bloc.dart';

abstract class PublicationState extends Equatable {
  const PublicationState();

  @override
  List<Object> get props => [];
}

class PublicationInitial extends PublicationState {}

class LoadingGetPublicationUid extends PublicationState {}

class LoadedGetPublicationUid extends PublicationState {
  LoadedGetPublicationUid({required this.posts});
  final List<GetPublicationId> posts;
  @override
  List<Object> get props => [posts];
}

class ExceptionGetPublicationUid extends PublicationState {
  ExceptionGetPublicationUid({required this.message});
  final String message;
  @override
  List<Object> get props => [message];
}
