import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../../core/error/failure_message.dart';
import '../../../../../core/error/failures.dart';
import '../../../data/models/request/get_publication_request.dart';
import '../../../domain/entities/get_publication_uid.dart';
import '../../../domain/usecases/get_publication_uid_us.dart';

part 'publication_event.dart';
part 'publication_state.dart';

class PublicationBloc extends Bloc<PublicationEvent, PublicationState> {
  final GetPublicationUidCases _getPublicationUidCases;

  PublicationBloc({required GetPublicationUidCases getPublicationUidCases})
      : _getPublicationUidCases = getPublicationUidCases,
        super(PublicationInitial()) {
    on<GetPublicationUidEvent>(_onGetPublicationUid);
  }

  Future<void> _onGetPublicationUid(
    GetPublicationUidEvent event,
    Emitter<PublicationState> emit,
  ) async {
    emit(LoadingGetPublicationUid());
    //! Llamando a la Api
    final response = await _getPublicationUidCases(
        GetPublicationParams(userId: event.userId));
    //! Transformamos el resultado
    emit(_eitherGetUsersState(response));
  }

  PublicationState _eitherGetUsersState(
    Either<Failure, List<GetPublicationId>> failureOrUser,
  ) {
    return failureOrUser.fold(
      (failure) => ExceptionGetPublicationUid(
        message: failureToMessage(failure),
      ),
      (posts) => LoadedGetPublicationUid(posts: posts),
    );
  }
}
