import 'package:flutter/material.dart';

import '../../../../../core/framework/colors.dart';
import '../../../../../core/framework/properties.dart';
import '../../../domain/entities/get_publication_uid.dart';

void showSheetPublicationDetail(BuildContext context,
    {required ThemeData themeData,
    required Size size,
    required GetPublicationId post}) {
  showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(
        top: Radius.circular(RADIUS),
      ),
    ),
    builder: (context) => DraggableScrollableSheet(
      expand: false,
      initialChildSize: 0.9,
      maxChildSize: 0.9,
      minChildSize: 0.4,
      builder: (context, scrollController) => Column(
        children: [
          Container(
            width: 50,
            height: 10,
            margin: const EdgeInsets.symmetric(vertical: 10),
            decoration: BoxDecoration(
              color: themeData.primaryColor,
              borderRadius: BorderRadius.circular(RADIUS),
            ),
          ),
          Expanded(
            child: Container(
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20, vertical: 20),
                    child: Text(
                      "${post.title}",
                      style: themeData.textTheme.titleMedium?.copyWith(
                        color: themeData.primaryColor,
                        fontSize: 25,
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Expanded(
                    child: Container(
                        width: size.width,
                        padding: const EdgeInsets.symmetric(
                            horizontal: 20, vertical: 20),
                        color: CONTENT_BODY_BACKGROUND_COLOR.withOpacity(0.2),
                        child: Text(
                          "${post.body}",
                          style: themeData.textTheme.titleMedium?.copyWith(
                            color: BLACK_COLOR.withOpacity(0.5),
                            fontSize: 20,
                          ),
                        )),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    ),
  );
}
