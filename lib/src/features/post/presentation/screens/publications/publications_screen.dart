import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../../core/framework/properties.dart';
import '../../../../../widgets/custom_item_user_detail.dart';
import '../../../../user/domain/entities/get_users.dart';
import '../../../../user/presentation/screens/widgets/loading.dart';
import '../../../domain/entities/get_publication_uid.dart';
import '../../bloc/publication/publication_bloc.dart';
import 'publication_view.dart';

class PublicationScreen extends StatelessWidget {
  const PublicationScreen({super.key});

  Future<void> getPublications(
    BuildContext context, {
    required int id,
  }) async {
    context.read<PublicationBloc>().add(GetPublicationUidEvent(id));
  }

  @override
  Widget build(BuildContext context) {
    final themedata = Theme.of(context);
    final size = MediaQuery.of(context).size;

    final args = ModalRoute.of(context)?.settings.arguments as GetUsers;

    return Scaffold(
      backgroundColor: themedata.backgroundColor,
      body: SafeArea(
        child: Container(
          child: FutureBuilder<Object?>(
              future: getPublications(context, id: args.id),
              builder: (context, snapshot) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomButtonBack(
                      size: size,
                      themedata: themedata,
                    ),
                    Divider(color: themedata.primaryColor, height: 20),
                    Container(
                      width: size.width,
                      height: 130,
                      alignment: Alignment.bottomCenter,
                      padding: const EdgeInsets.symmetric(horizontal: 30),
                      child: ListView(
                        physics: ClampingScrollPhysics(),
                        children: [
                          Text(
                            "${args.name} ${args.username}",
                            style: themedata.textTheme.titleMedium?.copyWith(
                              color: themedata.primaryColor,
                              fontSize: 17,
                            ),
                          ),
                          SizedBox(height: 10),
                          CustomUserDetailItem(
                            icon: Ionicons.call,
                            data: "${args.phone}",
                          ),
                          CustomUserDetailItem(
                            icon: Ionicons.mail,
                            data: "${args.email}",
                          )
                        ],
                      ),
                    ),
                    Divider(color: themedata.primaryColor, height: 20),
                    Expanded(
                      child: CustomPublicationsUserlist(
                        size: size,
                        themedata: themedata,
                      ),
                    ),
                  ],
                );
              }),
        ),
      ),
    );
  }
}

class CustomPublicationsUserlist extends StatelessWidget {
  const CustomPublicationsUserlist({
    Key? key,
    required this.themedata,
    required this.size,
  }) : super(key: key);

  final ThemeData themedata;
  final Size size;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PublicationBloc, PublicationState>(
      builder: (context, state) {
        if (state is LoadingGetPublicationUid) {
          return LoadingCompoenet();
        } else if (state is LoadedGetPublicationUid) {
          if (state.posts.isEmpty) {
            return Center(
                child: Text(
              "List is empty",
              style: themedata.textTheme.labelSmall?.copyWith(fontSize: 30),
            ));
          }
          return Container(
            padding: const EdgeInsets.only(left: 10, right: 10, bottom: 20),
            child: GridView.builder(
              gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                maxCrossAxisExtent: 200,
                mainAxisExtent: 300,
                childAspectRatio: 3 / 2,
                crossAxisSpacing: 10,
              ),
              physics: ClampingScrollPhysics(),
              itemCount: state.posts.length,
              itemBuilder: (_, index) => FadeIn(
                child: _PublicationDetail(
                  size: size,
                  themeData: themedata,
                  post: state.posts[index],
                ),
              ),
            ),
          );
        } else {
          return Container(
            child: Text("ERROR"),
          );
        }
      },
    );
  }
}

class CustomButtonBack extends StatelessWidget {
  const CustomButtonBack({
    Key? key,
    required this.themedata,
    required this.size,
  }) : super(key: key);
  final ThemeData themedata;
  final Size size;
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          InkWell(
            onTap: () => Navigator.pop(context),
            child: Container(
              width: 100,
              height: 50,
              alignment: Alignment.center,
              margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              decoration: BoxDecoration(
                color: themedata.primaryColor,
                borderRadius: BorderRadius.circular(RADIUS_CIRCLE),
              ),
              child: Icon(Ionicons.arrow_back),
            ),
          ),
          Container(
              padding: EdgeInsets.only(right: 20),
              child: Text(
                "PUBLICACIONES",
                style: themedata.textTheme.titleMedium?.copyWith(
                  color: themedata.primaryColor,
                  fontSize: 15,
                ),
              ))
        ],
      ),
    );
  }
}

class _PublicationDetail extends StatelessWidget {
  const _PublicationDetail({
    Key? key,
    required this.post,
    required this.themeData,
    required this.size,
  }) : super(key: key);

  final ThemeData themeData;
  final Size size;
  final GetPublicationId post;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: (() => showSheetPublicationDetail(context,
          themeData: themeData, size: size, post: post)),
      child: Container(
        height: 200,
        padding: const EdgeInsets.symmetric(vertical: 5),
        child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(RADIUS),
          ),
          child: ListTile(
            contentPadding: const EdgeInsets.all(20),
            title: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10),
              child: Text(
                "${post.title}",
                style: themeData.textTheme.titleMedium?.copyWith(
                  color: themeData.primaryColor,
                  fontSize: 17,
                ),
              ),
            ),
            subtitle: Text(
              post.body,
              maxLines: 4,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
      ),
    );
  }
}
