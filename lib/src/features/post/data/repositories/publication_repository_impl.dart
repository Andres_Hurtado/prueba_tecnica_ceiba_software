import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../../../core/utils/http_get_response.dart';
import '../../domain/entities/get_publication_uid.dart';
import '../../domain/repositories/publication_repository.dart';
import '../datasources/remote/publication_remote_data_sources.dart';
import '../models/request/get_publication_request.dart';

class PublicationRepositoryImpl implements PublicationRepository {
  PublicationRepositoryImpl({
    required NetworkInfo networkInfo,
    required PublicationRemoteDataSource publicationRemoteDataSource,
  })  : _publicationRemoteDataSource = publicationRemoteDataSource,
        _networkInfo = networkInfo;
  final NetworkInfo _networkInfo;
  final PublicationRemoteDataSource _publicationRemoteDataSource;

  @override
  Future<Either<Failure, List<GetPublicationId>>> getPPublications(
      GetPublicationParams param) {
    return Utils.responseRepositoryTempl(
      networkInfo: _networkInfo,
      param: param,
      method: _publicationRemoteDataSource.getPublications,
    );
  }
}
