import 'package:http/http.dart' as http;

import '../../../../../core/error/exceptions.dart';
import '../../../../../core/utils/http_get_response.dart';
import '../../models/request/get_publication_request.dart';
import '../../models/response/get_publication_model.dart';

abstract class PublicationRemoteDataSource {
  /// Llama al extremo http://url/.
  /// Lanza una [ServerException] para todos los códigos de error.
  Future<List<GetPublicationIdModel>> getPublications(
      GetPublicationParams param);
}

class PublicationRemoteDataSourceImpl implements PublicationRemoteDataSource {
  const PublicationRemoteDataSourceImpl({
    required this.client,
  });

  final http.Client client;

  @override
  Future<List<GetPublicationIdModel>> getPublications(
      GetPublicationParams param) async {
    final response = await Utils.callGetClient(
      client,
      endpoint: "posts?userId=${param.userId}",
    );
    return Utils.responseDataSourceTempl(
      response,
      data: getPublicationIdFromJson(response.body),
    );
  }
}
