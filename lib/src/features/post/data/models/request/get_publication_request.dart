import 'package:equatable/equatable.dart';

class GetPublicationParams extends Equatable {
  const GetPublicationParams({
    required this.userId,
  });

  final int userId;

  @override
  List<Object?> get props => [userId];
}
