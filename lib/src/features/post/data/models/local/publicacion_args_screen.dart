import 'package:equatable/equatable.dart';

class PublicationArgsScreen extends Equatable {
  final int id;

  PublicationArgsScreen(this.id);

  @override
  List<Object?> get props => [id];
}
