import 'dart:convert';

import '../../../domain/entities/get_publication_uid.dart';

List<GetPublicationIdModel> getPublicationIdFromJson(String str) =>
    List<GetPublicationIdModel>.from(
        // ignore: unnecessary_lambdas
        json.decode(str).map((x) => GetPublicationIdModel.fromJson(x)));

class GetPublicationIdModel extends GetPublicationId {
  const GetPublicationIdModel({
    required super.userId,
    required super.id,
    required super.title,
    required super.body,
  });

  factory GetPublicationIdModel.fromJson(Map<String, dynamic> json) =>
      GetPublicationIdModel(
        userId: json["userId"],
        id: json["id"],
        title: json["title"],
        body: json["body"],
      );
}
