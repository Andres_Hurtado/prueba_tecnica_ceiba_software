
import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../data/models/request/get_publication_request.dart';
import '../entities/get_publication_uid.dart';

abstract class PublicationRepository {
  /// Metodo para obtener los usuarios
  Future<Either<Failure, List<GetPublicationId>>> getPPublications(GetPublicationParams param);
  
}
