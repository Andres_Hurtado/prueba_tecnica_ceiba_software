import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../data/models/request/get_publication_request.dart';
import '../entities/get_publication_uid.dart';
import '../repositories/publication_repository.dart';

class GetPublicationUidCases
    implements UseCase<List<GetPublicationId>, GetPublicationParams> {
  const GetPublicationUidCases(this._publicationRepository);
  final PublicationRepository _publicationRepository;

  @override
  Future<Either<Failure, List<GetPublicationId>>> call(
      GetPublicationParams param) async {
    return await _publicationRepository.getPPublications(param);
  }
}
