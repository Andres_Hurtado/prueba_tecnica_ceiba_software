import 'package:flutter/material.dart';
import 'package:loading_indicator/loading_indicator.dart';

import '../../../../../core/framework/colors.dart';

class LoadingCompoenet extends StatelessWidget {
  const LoadingCompoenet({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Container(
        width: 200,
        height: 200,
        child: LoadingIndicator(
          indicatorType: Indicator.ballPulseSync,
          colors: const [WHITE_COLOR],
          strokeWidth: 2,
        ),
      ),
    );
  }
}
