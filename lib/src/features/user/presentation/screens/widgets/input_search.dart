import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../../../../../core/framework/colors.dart';

class InputTextFieldSearch extends StatelessWidget {
  const InputTextFieldSearch({
    this.hintTitle = "",
    this.validator,
    this.onChanged,
    this.suffixIcon,
    this.prefixIcon,
    this.scrollPadding = false,
    required this.textController,
    this.enabledKeyBoard = true,
    this.onFieldSubmitted,
    super.key,
  });

  final String hintTitle;
  final TextEditingController textController;
  final String? Function(String?)? validator;
  final Function(String)? onChanged;
  final void Function(String)? onFieldSubmitted;

  final Widget? suffixIcon;
  final Widget? prefixIcon;
  final bool scrollPadding;
  final bool enabledKeyBoard;
  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    return Container(
        width: double.infinity,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10.0),
        ),
        child: TextFormField(
          maxLines: 1,
          onFieldSubmitted: onFieldSubmitted,
          controller: textController,
          inputFormatters: [
            LengthLimitingTextInputFormatter(250),
          ],
          validator: validator,
          onChanged: onChanged,
          enabled: enabledKeyBoard,
          textInputAction: TextInputAction.search, // Moves focus to next.
          keyboardType: TextInputType.text,
          cursorColor: themeData.colorScheme.surface,
          style: themeData.textTheme.labelMedium?.copyWith(
            fontSize: 15,
            color: SECONDARY_TEXT_COLOR,
          ),
          decoration: InputDecoration(
            counterText: '',
            // labelStyle: themeData.textTheme.labelSmall?.copyWith(fontSize: 12),
            hintText: hintTitle,
            hintStyle: themeData.textTheme.titleMedium?.copyWith(
              fontSize: 15,
              color: themeData.primaryColor,
            ),
            border: OutlineInputBorder(
                gapPadding: 0,
                borderRadius: BorderRadius.circular(10.0),
                borderSide: BorderSide.none),
            fillColor: WHITE_COLOR,
            filled: true,
            suffixIcon: suffixIcon,
            prefixIcon: prefixIcon,
          ),
        ));
  }
}
