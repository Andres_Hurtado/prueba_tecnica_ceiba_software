import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../../core/framework/colors.dart';
import '../../../../../../core/framework/properties.dart';
import '../../../bloc/user/user_bloc.dart';
import '../../../helpers/search_deley_function.dart';
import '../../widgets/input_search.dart';

class SearchUsers extends StatefulWidget {
  const SearchUsers({
    Key? key,
    required this.size,
    required this.themeData,
  }) : super(key: key);
  final Size size;
  final ThemeData themeData;

  @override
  State<SearchUsers> createState() => _SearchUsersState();
}

class _SearchUsersState extends State<SearchUsers> {
  late TextEditingController _textSearch;
  late final SearchDebouncer _debouncer;

  @override
  void initState() {
    _textSearch = TextEditingController();
    //! Duracion al buscar un usuario
    _debouncer = SearchDebouncer(milliseconds: 500);

    super.initState();
  }

  @override
  void dispose() {
    _textSearch.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: widget.size.width,
      height: 55,
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      decoration: BoxDecoration(
        color: widget.themeData.colorScheme.secondary,
        border: Border.all(color: BORDER_COLOR.withOpacity(0.3)),
        borderRadius: BorderRadius.circular(RADIUS),
      ),
      child: InputTextFieldSearch(
        hintTitle: "Buscar Usuario",
        textController: _textSearch,
        onChanged: (text) => invoke(context, text),
      ),
    );
  }

  Future<void> invoke(BuildContext context, String text) async {
    _debouncer.run(() {
      context.read<UserBloc>().add(GetUserLocalEevent(text));

      // final
      // _userVloc = context.read()<UserBloc>();
      // if (text.isEmpty) {
      //   _userVloc.add(ActiveSearchCitiesEvent(isActive: false));
      // }
    });
  }
}
