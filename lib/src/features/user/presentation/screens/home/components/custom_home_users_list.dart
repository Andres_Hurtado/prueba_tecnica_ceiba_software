import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ionicons/ionicons.dart';

import '../../../../../../core/framework/colors.dart';
import '../../../../../../core/framework/properties.dart';
import '../../../../../../widgets/custom_item_user_detail.dart';
import '../../../../../post/presentation/routes/user_routes.dart';
import '../../../../domain/entities/get_users.dart';
import '../../../bloc/user/user_bloc.dart';
import '../../widgets/loading.dart';

class CustomUsersDetailList extends StatelessWidget {
  const CustomUsersDetailList({
    Key? key,
    required this.themeData,
  }) : super(key: key);
  final ThemeData themeData;

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        if (state is LoadingGetUser) {
          return LoadingCompoenet();
        } else if (state is LoadedGetUser) {
          if (state.users.isEmpty) {
            return ListIsEmpty(themeData: themeData);
          }
          return FadeInDown(
            
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: ListView.builder(
                physics: ClampingScrollPhysics(),
                itemCount: state.users.length,
                itemBuilder: (_, index) => FadeIn(
                  child: _UserDetail(
                    themedata: themeData,
                    user: state.users[index],
                  ),
                ),
              ),
            ),
          );
        } else {
          return _ErrorUsersList(themeData: themeData);
        }
      },
    );
  }
}

class _ErrorUsersList extends StatelessWidget {
  const _ErrorUsersList({
    Key? key,
    required this.themeData,
  }) : super(key: key);
  final ThemeData themeData;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(
              "Ooh! ocurrio un error, al obtener los usuarios",
              style: themeData.textTheme.titleMedium
                  ?.copyWith(color: BLACK_COLOR.withOpacity(0.4)),
              textAlign: TextAlign.center,
            ),
          ),
          IconButton(
              onPressed: () =>
                  context.read<UserBloc>().add(GetUserLocalEevent(null)),
              icon: Icon(
                Ionicons.refresh_outline,
                color: BLACK_COLOR,
              ))
        ],
      ),
    );
  }
}

class ListIsEmpty extends StatelessWidget {
  const ListIsEmpty({
    Key? key,
    required this.themeData,
  }) : super(key: key);

  final ThemeData themeData;

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Text(
      "List is empty",
      style: themeData.textTheme.labelSmall?.copyWith(fontSize: 30),
    ));
  }
}

class _UserDetail extends StatelessWidget {
  const _UserDetail({
    Key? key,
    required this.user,
    required this.themedata,
  }) : super(key: key);

  final ThemeData themedata;
  final GetUsers user;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      padding: const EdgeInsets.symmetric(vertical: 5),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(RADIUS),
        ),
        child: Stack(
          children: [
            ListTile(
              contentPadding: const EdgeInsets.all(20),
              title: Padding(
                padding: const EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  "${user.name} ${user.username}",
                  style: themedata.textTheme.titleMedium?.copyWith(
                    color: themedata.primaryColor,
                    fontSize: 17,
                  ),
                ),
              ),
              subtitle: Column(
                children: [
                  CustomUserDetailItem(
                    icon: Ionicons.call,
                    data: user.phone,
                  ),
                  SizedBox(height: 10),
                  CustomUserDetailItem(
                    icon: Ionicons.mail,
                    data: user.email,
                  ),
                ],
              ),
            ),
            Positioned(
              bottom: 20,
              right: 10,
              child: InkWell(
                onTap: () => Navigator.pushNamed(
                  context,
                  PublicationsRoutes.publications,
                  arguments: user,
                ),
                child: Container(
                  child: Text(
                    "VER PUBLICACIONES",
                    style: themedata.textTheme.titleSmall
                        ?.copyWith(color: themedata.primaryColor),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
