import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../core/framework/colors.dart';
import '../../bloc/user/user_bloc.dart';
import 'components/custom_home_search.dart';
import 'components/custom_home_users_list.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  Future<void> getUsers(BuildContext context) async {
    context.read<UserBloc>().add(GetUserLocalEevent(null));
  }

  @override
  Widget build(BuildContext context) {
    final themedata = Theme.of(context);
    final size = MediaQuery.of(context).size;

    return Scaffold(
        backgroundColor: themedata.backgroundColor,
        appBar: AppBar(
          elevation: 0,
          title: Text(
            'PRUEBA DE INGRESO',
            style: themedata.textTheme.titleMedium?.copyWith(
              color: PRIMARY_TEXT_COLOR,
            ),
          ),
        ),
        body: FutureBuilder<Object?>(
          future: getUsers(context),
          builder: (context, snapshot) {
            return Container(
              width: size.width,
              height: size.height,
              child: Column(
                children: [
                  SearchUsers(
                    size: size,
                    themeData: themedata,
                  ),
                  Expanded(
                    child: CustomUsersDetailList(
                      themeData: themedata,
                    ),
                  ),
                ],
              ),
            );
          },
        ));
  }
}
