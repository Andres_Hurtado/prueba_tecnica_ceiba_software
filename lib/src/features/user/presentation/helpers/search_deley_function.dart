import 'dart:async';
import 'package:flutter/material.dart';

class SearchDebouncer {
  final int milliseconds;
  late VoidCallback action;
  Timer _timer = Timer(Duration(milliseconds: 0), () {});

  SearchDebouncer({this.milliseconds = 0});

  Future<void> run(VoidCallback action) async {
    if (_timer.isActive) {
      _timer.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}
