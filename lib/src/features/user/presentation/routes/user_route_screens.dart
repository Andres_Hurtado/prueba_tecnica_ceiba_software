import 'package:flutter/material.dart';

import '../screens/home/home_screen.dart';
import 'user_routes.dart';

final userRoutes = <String, WidgetBuilder>{
  Routes.home: (_) => HomeScreen(),
};
