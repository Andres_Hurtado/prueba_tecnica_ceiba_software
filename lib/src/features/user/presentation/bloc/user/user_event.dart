part of 'user_bloc.dart';

abstract class UserEvent extends Equatable {
  const UserEvent();

  @override
  List<Object> get props => [];
}

class GetUserEevent extends UserEvent {}

class GetUserLocalEevent extends UserEvent {
  GetUserLocalEevent(this.text);
  final String? text;
}
