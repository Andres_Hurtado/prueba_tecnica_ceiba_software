import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';

import '../../../../../core/error/failure_message.dart';
import '../../../../../core/error/failures.dart';
import '../../../data/models/local/get_users_local_or_search.dart';
import '../../../data/models/local/users_local_model.dart';
import '../../../data/models/request/get_user_serach_params.dart';
import '../../../data/models/request/user_not_params.dart';
import '../../../data/models/request/users_local_params.dart';
import '../../../domain/entities/get_users.dart';
import '../../../domain/usecases/get_users_local_us.dart';
import '../../../domain/usecases/get_users_us.dart';
import '../../../domain/usecases/save_users_local_us.dart';

part 'user_event.dart';
part 'user_state.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  final GetUsersCases _getUsersCases;
  final SaveUsersLocalCases _saveUsersLocalCases;
  final GetUsersLocalCases _getUsersLocalCases;

  UserBloc({
    required GetUsersCases getUsersCases,
    required SaveUsersLocalCases saveUsersLocalCases,
    required GetUsersLocalCases getUsersLocalCases,
  })  : _getUsersCases = getUsersCases,
        _getUsersLocalCases = getUsersLocalCases,
        _saveUsersLocalCases = saveUsersLocalCases,
        super(UserInitial()) {
    on<GetUserEevent>(_onUserEvent);
    on<GetUserLocalEevent>(_onGetUserLocalEevent);
  }

  Future<void> _onUserEvent(
    GetUserEevent event,
    Emitter<UserState> emit,
  ) async {
    emit(LoadingGetUser());
    //! Llamando a la Api
    final response = await _getUsersCases(UsersNoParams());
    //! Transformamos el resultado
    emit(_eitherGetUsersState(response));
  }

  UserState _eitherGetUsersState(
    Either<Failure, List<GetUsers>> failureOrUser,
  ) {
    return failureOrUser.fold(
      (failure) => ExceptionGetUser(
        message: failureToMessage(failure),
      ),
      (users) {
        //! Creamos el lista de tipo GetUsersLocal
        //! para poderlo guardar de forma local
        var usersLocal = <GetUsersLocal>[];
        if (users.isNotEmpty) {
          usersLocal = users
              .map((element) => GetUsersLocal(
                    id: element.id,
                    name: element.name,
                    username: element.username,
                    email: element.email,
                    phone: element.phone,
                  ))
              .toList();
          //! Guardamos los usuarios de forma local
          _saveUsersLocalCases.call(UsersLocalParams(usersLocal));
        }
        //! Mostramos los usuario llamados desde la Api
        return LoadedGetUser(users: users);
      },
    );
  }

  Future<void> _onGetUserLocalEevent(
    GetUserLocalEevent event,
    Emitter<UserState> emit,
  ) async {
    emit(LoadingGetUser());
    //! Llamando a la data de forma local
    final response = await _getUsersLocalCases(
      UsersLocalSearchParams(text: event.text),
    );
    //! Transformamos el resultado
    emit(_eitherGetUsersLocalState(response));
  }

  UserState _eitherGetUsersLocalState(
    Either<Failure, GetUsersLocalOrSearch> failureOrUser,
  ) {
    return failureOrUser.fold(
      (failure) => ExceptionGetUser(
        message: failureToMessage(failure),
      ),
      (get) {
        //! LLamamos si los usuarios no estan registrados
        //! de forma local, para que se obtenga desde la Api
        if (get.users.isEmpty && get.isSearch == false) {
          add(GetUserEevent());
        }
        //! Mostramos los usuario llamados desde la Api
        return LoadedGetUser(users: get.users);
      },
    );
  }
}
