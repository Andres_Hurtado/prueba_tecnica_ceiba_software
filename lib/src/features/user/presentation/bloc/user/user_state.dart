part of 'user_bloc.dart';

abstract class UserState extends Equatable {
  const UserState();

  @override
  List<Object> get props => [];
}
class UserInitial extends UserState {}

class LoadingGetUser extends UserState {}

class LoadedGetUser extends UserState {
  LoadedGetUser({required this.users});
  final List<GetUsers> users;
  @override
  List<Object> get props => [users];
}

class ExceptionGetUser extends UserState {
  ExceptionGetUser({required this.message});
  final String message;
  @override
  List<Object> get props => [message];
}
