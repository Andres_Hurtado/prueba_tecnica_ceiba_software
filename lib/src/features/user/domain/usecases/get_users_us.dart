import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../data/models/request/user_not_params.dart';
import '../entities/get_users.dart';
import '../repositories/user_repository.dart';

class GetUsersCases implements UseCase<List<GetUsers>, UsersNoParams> {
  const GetUsersCases(this._userRepository);
  final UserRepository _userRepository;

  @override
  Future<Either<Failure, List<GetUsers>>> call(UsersNoParams param) async {
    return await _userRepository.getUsers();
  }
}
