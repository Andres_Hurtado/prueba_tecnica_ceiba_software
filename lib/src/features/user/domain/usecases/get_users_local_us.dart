import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../data/models/local/get_users_local_or_search.dart';
import '../../data/models/request/get_user_serach_params.dart';
import '../repositories/user_repository.dart';

class GetUsersLocalCases implements UseCase<GetUsersLocalOrSearch, UsersLocalSearchParams> {
  const GetUsersLocalCases(this._userRepository);
  final UserRepository _userRepository;

  @override
  Future<Either<Failure, GetUsersLocalOrSearch>> call(UsersLocalSearchParams param) async {
    return await _userRepository.getUsersLocal(param);
  }
}
