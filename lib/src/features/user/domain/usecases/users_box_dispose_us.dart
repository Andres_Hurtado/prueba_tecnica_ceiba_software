import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../data/models/request/user_not_params.dart';
import '../repositories/user_repository.dart';

class UsersBoxDisposeCases implements UseCase<bool, UsersNoParams> {
  const UsersBoxDisposeCases(this._userRepository);
  final UserRepository _userRepository;

  @override
  Future<Either<Failure, bool>> call(UsersNoParams param) async {
    return await _userRepository.disposeBox();
  }
}
