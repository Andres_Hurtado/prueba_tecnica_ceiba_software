import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/usecases/usecase.dart';
import '../../data/models/request/users_local_params.dart';
import '../repositories/user_repository.dart';

class SaveUsersLocalCases implements UseCase<bool, UsersLocalParams> {
  const SaveUsersLocalCases(this._userRepository);
  final UserRepository _userRepository;

  @override
  Future<Either<Failure, bool>> call(UsersLocalParams param) async {
    return await _userRepository.saveUsersLocal(param);
  }
}
