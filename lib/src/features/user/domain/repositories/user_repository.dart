import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../data/models/local/get_users_local_or_search.dart';
import '../../data/models/request/get_user_serach_params.dart';
import '../../data/models/request/users_local_params.dart';
import '../entities/get_users.dart';

abstract class UserRepository {
  /// Metodo para obtener los usuarios
  Future<Either<Failure, List<GetUsers>>> getUsers();
  Future<Either<Failure, GetUsersLocalOrSearch>> getUsersLocal(UsersLocalSearchParams param);
  Future<Either<Failure, bool>> saveUsersLocal(UsersLocalParams params);
  Future<Either<Failure, bool>> disposeBox();
}
