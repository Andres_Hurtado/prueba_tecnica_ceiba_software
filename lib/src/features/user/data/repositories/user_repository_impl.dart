import 'package:dartz/dartz.dart';

import '../../../../core/error/failures.dart';
import '../../../../core/network/network_info.dart';
import '../../../../core/utils/http_get_response.dart';
import '../../domain/entities/get_users.dart';
import '../../domain/repositories/user_repository.dart';
import '../datasources/local/user_local_data_source.dart';
import '../datasources/remote/user_remote_data_source.dart';
import '../models/local/get_users_local_or_search.dart';
import '../models/request/get_user_serach_params.dart';
import '../models/request/users_local_params.dart';

class UserRepositoryImpl implements UserRepository {
  UserRepositoryImpl(
      {required NetworkInfo networkInfo,
      required UserRemoteDataSource userRemoteDataSource,
      required UserLocalDataSource userLocalDataSource})
      : _userRemoteDataSource = userRemoteDataSource,
        _userLocalataSource = userLocalDataSource,
        _networkInfo = networkInfo;
  final NetworkInfo _networkInfo;
  final UserRemoteDataSource _userRemoteDataSource;
  final UserLocalDataSource _userLocalataSource;

  @override
  Future<Either<Failure, List<GetUsers>>> getUsers() {
    return Utils.responseRepositoryTempl(
      networkInfo: _networkInfo,
      param: null,
      method: _userRemoteDataSource.getUsers,
    );
  }

  @override
  Future<Either<Failure, GetUsersLocalOrSearch>> getUsersLocal(UsersLocalSearchParams param) {
    return Utils.responseRepositoryLocalTempl(
      param: param,
      method: _userLocalataSource.getUsersLocal,
    );
  }

  @override
  Future<Either<Failure, bool>> saveUsersLocal(UsersLocalParams param) {
    return Utils.responseRepositoryLocalTempl(
      param: param,
      method: _userLocalataSource.saveUsersLocal,
    );
  }

  @override
  Future<Either<Failure, bool>> disposeBox() async {
    return Utils.responseRepositoryLocalTempl(
      param: null,
      method: _userLocalataSource.saveUsersLocal,
    );
  }
}
