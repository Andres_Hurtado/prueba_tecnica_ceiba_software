import 'package:http/http.dart' as http;

import '../../../../../core/error/exceptions.dart';
import '../../../../../core/utils/http_get_response.dart';
import '../../models/response/get_users_model.dart';

abstract class UserRemoteDataSource {
  /// Llama al extremo http://url/.
  /// Lanza una [ServerException] para todos los códigos de error.
  Future<List<GetUsersModel>> getUsers();
}

class UserRemoteDataSourceImpl implements UserRemoteDataSource {
  const UserRemoteDataSourceImpl({
    required this.client,
  });

  final http.Client client;

  @override
  Future<List<GetUsersModel>> getUsers() async {
    final response = await Utils.callGetClient(
      client,
      endpoint: "users",
    );

    return Utils.responseDataSourceTempl(
      response,
      data: getUserFromJson(response.body),
    );
  }
}
