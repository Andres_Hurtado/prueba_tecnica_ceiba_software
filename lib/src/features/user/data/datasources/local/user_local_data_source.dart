import '../../../../../core/error/exceptions.dart';
import '../../../../../core/error/failures.dart';
import '../../../../../core/inilicilize_hive_box/inilicialize_hive_box.dart';
import '../../models/local/get_users_local_or_search.dart';
import '../../models/local/users_local_model.dart';
import '../../models/request/get_user_serach_params.dart';
import '../../models/request/users_local_params.dart';
import '../../models/response/get_users_model.dart';

abstract class UserLocalDataSource {
  /// Lanza una [LocalException] para todos los códigos de error.
  Future<GetUsersLocalOrSearch> getUsersLocal(UsersLocalSearchParams param);

  /// Lanza una [LocalException] para todos los códigos de error.
  Future<bool> saveUsersLocal(UsersLocalParams params);

  /// Lanza una [LocalException] para todos los códigos de error.
  Future<bool> disposeBox(UsersLocalParams params);
}

class UserLocalDataSourceeImpl implements UserLocalDataSource {
  UserLocalDataSourceeImpl({
    required InitializeBox initializeBox,
  }) : _initializeBox = initializeBox;
  final InitializeBox _initializeBox;

  @override
  Future<GetUsersLocalOrSearch> getUsersLocal(
      UsersLocalSearchParams param) async {
    var users = <GetUsersModel>[];

    var box = await _initializeBox.initializeBox;
    final result = box
        .toMap()
        .map(
          (k, e) => MapEntry(
            k.toString(),
            GetUsersLocal(
              id: e.id,
              name: e.name,
              email: e.email,
              phone: e.phone,
              username: e.username,
            ),
          ),
        )
        .values
        .toList();

    //! conversion de GetUsersLocal a GetUsersModel
    users = result.map(GetUsersModel.fromJsonDataLocal).toList();
    if (param.text != null) {
      users = users.where((element) {
        var text = param.text?.toLowerCase();
        return element.name.toLowerCase().contains(text!) ||
            element.username.toLowerCase().contains(text);
      }).toList();
    }

    return GetUsersLocalOrSearch(users: users, isSearch: param.text != null);
  }

  @override
  Future<bool> saveUsersLocal(UsersLocalParams params) async {
    try {
      var box = await _initializeBox.initializeBox;
      await box.addAll(params.users);
      return true;
    } on LocalException catch (_) {
      throw LocalFailure(exception: "No se logro realizar el guardado");
    }
  }

  @override
  Future<bool> disposeBox(UsersLocalParams params) async {
    var box = await _initializeBox.initializeBox;
    box.close();
    return true;
  }
}
