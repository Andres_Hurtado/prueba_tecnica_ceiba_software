import 'package:hive/hive.dart';

part 'users_local_model.g.dart';

//! Base de datos no relacional a diferencia de SQLITE
@HiveType(typeId: 0)
class GetUsersLocal extends HiveObject {
  GetUsersLocal({
    required this.id,
    required this.name,
    required this.username,
    required this.email,
    required this.phone,
  });

  @HiveField(0)
  final int id;
  @HiveField(1)
  final String name;
  @HiveField(2)
  final String username;
  @HiveField(3)
  final String email;
  @HiveField(4)
  final String phone;
}
