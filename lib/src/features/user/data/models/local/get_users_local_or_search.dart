import 'package:equatable/equatable.dart';

import '../response/get_users_model.dart';

class GetUsersLocalOrSearch extends Equatable {
  final List<GetUsersModel> users;
  final bool isSearch;

  const GetUsersLocalOrSearch({required this.users, required this.isSearch});

  @override
  List<Object?> get props => [users, isSearch];
}
