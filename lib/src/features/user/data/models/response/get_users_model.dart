import 'dart:convert';

import '../../../domain/entities/get_users.dart';
import '../local/users_local_model.dart';

List<GetUsersModel> getUserFromJson(String str) => List<GetUsersModel>.from(
    // ignore: unnecessary_lambdas
    json.decode(str).map((x) => GetUsersModel.fromJson(x)));

class GetUsersModel extends GetUsers {
  const GetUsersModel({
    required super.id,
    required super.name,
    required super.username,
    required super.email,
    required super.phone,
  });

  factory GetUsersModel.fromJson(Map<String, dynamic> json) => GetUsersModel(
        id: json["id"],
        name: json["name"],
        username: json["username"],
        email: json["email"],
        phone: json["phone"],
      );

  factory GetUsersModel.fromJsonDataLocal(GetUsersLocal local) => GetUsersModel(
        id: local.id,
        name: local.name,
        username: local.username,
        email: local.email,
        phone: local.phone,
      );
}
