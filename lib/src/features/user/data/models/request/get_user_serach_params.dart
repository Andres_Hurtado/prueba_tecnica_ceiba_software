import 'package:equatable/equatable.dart';


class UsersLocalSearchParams extends Equatable {
  UsersLocalSearchParams({this.text});
  final String? text;
  @override
  List<Object?> get props => [text];
}
