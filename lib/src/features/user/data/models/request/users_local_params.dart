import 'package:equatable/equatable.dart';

import '../local/users_local_model.dart';

class UsersLocalParams extends Equatable {
  UsersLocalParams(this.users);
  final List<GetUsersLocal> users;
  @override
  List<Object?> get props => [users];
}
